import FightersView from './fightersView';
import Fighter from './Fighter';
import FightView from './fightView';

function delay() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve ();
        }, 800);
    });
}

async function winnerAnnouncement(winner) {

    const FightView = document.querySelector('.fight-area');
    const winnerTitle = document.createElement('p');
    winnerTitle.innerText = `${winner} wins!`;
    winnerTitle.classList.add('winner-title');
    FightView.append(winnerTitle);
    await delay();
    winnerTitle.style.transform = 'translate(-50%, -50%)';
}

function healthLeft(domElement, left, initial) {
    domElement.innerText = left < 0 ? 0 : left;
    domElement.style.width = `${Math.floor((left / initial) * 100)}%`;
}

function lastUIupdate(domElement, initial, className) {
    healthLeft(domElement, 0, initial);
    domElement.classList.remove(className);
}

function damageScore(attack, defence) {
    let damage = attack - defence;
    if (damage < 0) damage = 0;
    return damage;
}

function hit(attacker, defender) {
    return new Promise(resolve => {
        setTimeout(() => {
            defender.health -= damageScore(attacker.getHitPower(), defender.getBlockPower());
            resolve (defender.health);
        }, 700);
    });
}

async function fight(first, second) {
    const initialFirstHealth = first.health;
    const initialSecondHealth = second.health;
    let winner = null;

    const startAudio = new Audio("../../resources/fight_start.mp3");
    startAudio.play();

    new FightView("create", first, second);

    const firstFighterHealth = document.getElementById('first-fighter-health');
    const secondFighterHealth = document.getElementById('second-fighter-health');

    while (1) {
        if (first.health <= 0) {
            lastUIupdate(firstFighterHealth, initialFirstHealth, 'blue');
            winner = second.name;
            break ;
        }
        if (second.health <= 0) {
            lastUIupdate(secondFighterHealth, initialSecondHealth, 'red');
            winner = first.name;
            break ;
        }

        const secondHealthLeft = await hit(first, second);
        healthLeft(secondFighterHealth, secondHealthLeft, initialSecondHealth);

        const firstHealthLeft = await hit(second, first);
        healthLeft(firstFighterHealth, firstHealthLeft, initialFirstHealth);
    }

    winnerAnnouncement(winner);
    const toMainMenuBtn = document.querySelector('.main-menu');
    toMainMenuBtn.style.visibility = 'visible';
    toMainMenuBtn.addEventListener('click', event => new FightView('delete'), false);
    startAudio.pause();

    const winnerAudio = new Audio("../../resources/victory.mp3");
    winnerAudio.play();
}

function fightStart(event) {
    const selectedFighters = document.querySelectorAll('.checked-fighter:checked');
    const skillsModalError = document.querySelector('.skills-modal-error');
    const modalError = document.getElementById('modal-error');
    const error = document.getElementById('error-message');

    if (selectedFighters.length !== 2) {
        skillsModalError.classList.add('open');
        modalError.style.visibility = 'visible';
        error.innerHTML = `Select 2 fighters to start the battle. You have chosen - ${selectedFighters.length}`;

        return ;
    }

    const [ firstId, secondId ] = [...selectedFighters].map((el) => el.getAttribute('data-id'));
    const firstFighter = FightersView.fightersDetailsMap.get(firstId);
    const secondFighter = FightersView.fightersDetailsMap.get(secondId);
    fight(new Fighter(firstFighter), new Fighter(secondFighter));
}

export { fightStart }