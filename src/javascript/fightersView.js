import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  static modalError = document.getElementById('modal-error');
  static skillsModalError = document.querySelector('.skills-modal-error');
  static modalOverlay = document.getElementById('modal-overlay');
  static inputHealth = document.getElementById('input-health');
  static inputAttack = document.getElementById('input-attack');
  static inputDefense = document.getElementById('input-defense');
  static updateSkillsBtn = document.querySelector('.update-skills');
  static nameFighter = document.querySelector('.name-fighter');
  static skillsModal = document.querySelector('.skills-modal');
  static fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);

    FightersView.modalOverlay.addEventListener('click', event => this.handleModalOverlayClick(event), false);
    FightersView.modalError.addEventListener('click', event => this.handleModalErrorOverlayClick(event), false);
  }

  showModal(fighter) {
    const { name, health, attack, defense, _id } = fighter;

    FightersView.modalOverlay.style.visibility = 'visible';
    FightersView.skillsModal.classList.add('open');

    FightersView.nameFighter.innerHTML  = name;
    FightersView.inputHealth.value = health;
    FightersView.inputAttack.value = attack;
    FightersView.inputDefense.value = defense;
    FightersView.updateSkillsBtn.dataset.id = _id;
  }

  handleModalOverlayClick(event) {
    const target = event.target;

    if (target.id === 'modal-overlay') {
      FightersView.skillsModal.classList.remove('open');
      target.style.visibility = 'hidden';
    } else if (target.className === 'update-skills') {
      const updatedSkills = {
        health: +FightersView.inputHealth.value,
        attack: +FightersView.inputAttack.value,
        defense: +FightersView.inputDefense.value
      };

      const updatedFighter = { ...FightersView.fightersDetailsMap.get(target.dataset.id), ...updatedSkills };
      FightersView.fightersDetailsMap.set(target.dataset.id, updatedFighter);
      FightersView.skillsModal.classList.remove('open');
      target.parentNode.parentNode.style.visibility = 'hidden';
    }
  }

  handleModalErrorOverlayClick(event) {
    const target = event.target;

    FightersView.skillsModalError.classList.remove('open');
    target.style.visibility = 'hidden';
  }

  async handleFighterClick(event, fighter) {
    try {
      const { _id } = fighter;
      const selectedFighters = document.querySelectorAll('.checked-fighter:checked');

      if (!FightersView.fightersDetailsMap.has(_id)) {
        const fighterWithDetails = await fighterService.getFighterDetails(_id);
        FightersView.fightersDetailsMap.set(_id, fighterWithDetails);
      }

      if (event.target.className !== 'checked-fighter') {
        this.showModal(FightersView.fightersDetailsMap.get(_id));
      }
    } catch {
      console.warn(error);
    }
  }
}

export default FightersView;