import View from './view';

class FightView extends View {
    constructor(action, first, second) {
        super();

        if (action === 'create') {
            this.createFightView(first, second);
        }

        if (action === 'delete')
            this.deleteFightView();
    }

    static rootElement = document.getElementById('root');
    static startBtn = document.querySelector('#start-game');

    createFightView(first, second) {
        document.querySelector('.fighters').style.display = 'none';
        FightView.startBtn.style.display = 'none';

        FightView.rootElement.className = 'fight-background';

        this.element = this.createElement({ tagName: 'div', className: 'fight-area' });

        const toMainMenuBtn = this.createButton('main-menu', 'Main menu');

        const firstFighterDiv = this.createElement({ tagName: 'div', className: 'fighter-big'});
        const secondFighterDiv = this.createElement({ tagName: 'div', className: 'fighter-big'});

        firstFighterDiv.innerHTML = this.createHealthIndicator('blue', first.name,'first-fighter-health');
        firstFighterDiv.append(this.createImage(first.source, 'fighter-image-big'));

        secondFighterDiv.innerHTML = this.createHealthIndicator('red', second.name,'second-fighter-health');
        secondFighterDiv.append(this.createImage(second.source, 'fighter-image-big', 'fighter2-image-big'));

        this.element.append(toMainMenuBtn, firstFighterDiv, secondFighterDiv);
        FightView.rootElement.appendChild(this.element);
    }

    createHealthIndicator(color, name, id) {
        const healthBar = `<div class="fighters-name">${name}</div><div class="light round">
        <div class="container ${color} round" id="${id}"
        style="width:100%; margin-bottom: 60px;">Full health</div></div>`;

        return healthBar;
    }

    deleteFightView() {
        const selectedFighters = document.querySelectorAll('.checked-fighter:checked');
        [...selectedFighters].forEach( el => { el.checked = false; })
        document.querySelector('.fighters').style.display = 'flex';
        FightView.rootElement.className = '';
        FightView.startBtn.style.display = 'inline';
        document.querySelector('.fight-area').remove();
    }
}

export default FightView;