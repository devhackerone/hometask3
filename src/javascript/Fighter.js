class Fighter {
    constructor({ name, health, attack, defense, source }) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.source = source;
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min; //Возвращает случайное целое число между min (включительно) и max (не включая max)
    }

    getHitPower() {
        const criticalHitChance = this.getRandomInt(1, 3);
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        const dodgeChance = this.getRandomInt(1, 3);
        return this.defense * dodgeChance;
    }
}

export default Fighter;