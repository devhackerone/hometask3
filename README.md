# stage-2-es6-for-everyone

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/


Завдання
Можна продовжити у своєму репозиторії (якщо теж складав його по ходу лекції), а можна і склонувати готовий стартер. А далі: 1. У класі FighterService створити метод для отримання детальної інформації про бійця. Додати можливість її перегляду — для цього реалізувати метод, який би опрацьовував клік по бійцю, і якщо інформації про нього ще немає на клієнті, то надіслати запит і записати інформацію у колекцію. Дані можна відобразити у вигляді невеликого діалогового вікна з можливістю їх редагування. 2. Створити клас Fighter, який міститиме інформацію, необхідну бійцю для бою (name, health, attack, defense, etc.), а також методи

getHitPower, який би розраховував силу удару (кількість завданої шкоди здоров'ю противника) за формулою power = attack * criticalHitChance;, де criticalHitChance — рандомне число від 1 до 2,

getBlockPower, який би розраховував силу блоку (амортизація удару противника) за формулою power = defense * dodgeChance;, де dodgeChance — рандомне число від 1 до 2.

Створити функцію fight, яка прийматиме бійців як параметри та запускатиме процес гри. Гравці наносять удари один одному, а рівень їхнього health зменшується на getHitPower - getBlockPower (або ж 0, якщо боєць "ухилився" від удару повністю). Якщо один із них помирає, то гра закінчується й ім'я переможця виводиться на екран. (Хід бійки може бути довільним — наприклад, бійці можуть наносити удари по черзі чи з певним інтервалом, або ж можуть це робити за допомогою певного UI елементу на сторінці)

Cтворити додаткові елементи для реалізації гри — елемент для вибору бійців для бійки (може бути звичайний ""), кнопка, яка починає бійку, індикатор здоров'я, тощо.

Завдання необхідно виконувати у Bitbucket репозиторіях (не Github)